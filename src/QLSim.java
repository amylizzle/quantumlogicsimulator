import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import MathHelpers.*;
import QuantumGates.*;

public class QLSim extends JFrame{

	private static final long serialVersionUID = -5740165226253153450L;
	private QuantumCircuit QC;
	private JPanel circuitPanel;
	int cursorxpos,cursorypos;
	
	public QLSim()
	{
		cursorxpos=0;
		cursorypos=0;
		QC = new QuantumCircuit();
		this.setTitle("Quantum Logic Circuit Simulator");
		JPanel componentPanel = new JPanel();
		circuitPanel = new JPanel();
		JPanel controlPanel = new JPanel();
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		componentPanel.setLayout(new FlowLayout());
		//circuitPanel.setLayout(new GridLayout(1,1));
		controlPanel.setLayout(new FlowLayout());
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 0;
		c.weightx = 1.0;
		
		add(componentPanel,c);
		c.weighty = 1.0;

		c.gridy = 1;
		add(circuitPanel,c);
		c.weighty = 0;
		c.gridy = 2;
		add(controlPanel,c);
		
		JButton calculateBtn = new JButton("Calculate");
		JButton addLayerBtn = new JButton("Add Layer");
		JButton addRowBtn = new JButton("Add Row");
		JButton leftBtn = new JButton("<");
		JButton upBtn = new JButton("^");
		JButton downBtn = new JButton("V");
		JButton rightBtn = new JButton(">");
		JButton clearBtn = new JButton("Clear circuit");
		
		controlPanel.add(calculateBtn);
		controlPanel.add(addLayerBtn);
		controlPanel.add(addRowBtn);
		controlPanel.add(leftBtn);
		controlPanel.add(upBtn);
		controlPanel.add(downBtn);
		controlPanel.add(rightBtn);
		controlPanel.add(clearBtn);
		
		
		
		JButton HGateBtn = new JButton("H");
		JButton CNOTGateBtn = new JButton("CNOT");
		JButton PhaseGateBtn = new JButton("Phase");
		JButton TGateBtn = new JButton("T");
		JButton XGateBtn = new JButton("X");
		JButton YGateBtn = new JButton("Y");
		JButton ZGateBtn = new JButton("Z");
		JButton WireGateBtn = new JButton("Wire");
		JButton Q1GateBtn = new JButton("|1>");
		JButton Q0GateBtn = new JButton("|0>");
		
		componentPanel.add(HGateBtn);
		componentPanel.add(CNOTGateBtn);
		componentPanel.add(PhaseGateBtn);
		componentPanel.add(TGateBtn);
		componentPanel.add(XGateBtn);
		componentPanel.add(YGateBtn);
		componentPanel.add(ZGateBtn);
		componentPanel.add(WireGateBtn);
		componentPanel.add(Q1GateBtn);
		componentPanel.add(Q0GateBtn);
		
		HGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new HadamardGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		CNOTGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new CNOTGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		PhaseGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new PhaseGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		TGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new TGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		XGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new XGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		YGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new YGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		ZGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new ZGate());
				renderQCtoPanel(circuitPanel);
			}			
		});
		WireGateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new Wire());
				renderQCtoPanel(circuitPanel);
			}			
		});
		Q1GateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new Qubit(new ComplexNumber(0,0), new ComplexNumber(1,0)));
				renderQCtoPanel(circuitPanel);
			}			
		});
		Q0GateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.setGateAt(cursorypos, cursorxpos, new Qubit(new ComplexNumber(1,0), new ComplexNumber(0,0)));
				renderQCtoPanel(circuitPanel);
			}			
		});
		
		calculateBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				try {
					JOptionPane.showMessageDialog(null,"Computed result: \n"+QC.getFriendlyOutputState(),"Quantum Logic Simulator",JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,"Error computing result!\nError was "+e.getClass().getName(),"Quantum Logic Simulator",JOptionPane.WARNING_MESSAGE);
					e.printStackTrace();
				}
			}			
		});
		
		addLayerBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.addLayer();
				renderQCtoPanel(circuitPanel);
			}			
		});
		addRowBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC.addRow();
				renderQCtoPanel(circuitPanel);
			}
		});
		
		
		rightBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(cursorxpos < QC.getLayerCount()-1)
				cursorxpos++;
				renderQCtoPanel(circuitPanel);
			}			
		});
		leftBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(cursorxpos > 0)
					cursorxpos--;
				renderQCtoPanel(circuitPanel);
			}			
		});
		upBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(cursorypos > 0)
					cursorypos--;
				renderQCtoPanel(circuitPanel);
			}			
		});
		downBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(cursorypos < QC.getLayerSize()-1)
					cursorypos++;
				renderQCtoPanel(circuitPanel);
			}			
		});
		
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				QC = new QuantumCircuit();
				cursorxpos = 0;
				cursorypos = 0; 
				renderQCtoPanel(circuitPanel);
			}			
		});
		
		//loads a bell state generator as default circuit
		QC.setGateAt(0, 0, new Qubit(new ComplexNumber(1,0),new ComplexNumber(0,0)));
		QC.addRow();
		QC.setGateAt(1, 0, new Qubit(new ComplexNumber(1,0),new ComplexNumber(0,0)));
		QC.addLayer();
		QC.setGateAt(0, 1, new HadamardGate());
		QC.setGateAt(1, 1, new Wire());
		QC.addLayer();
		QC.setGateAt(0, 2, new CNOTGate());
		
		
		
		renderQCtoPanel(circuitPanel);
		
		pack();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
	}
	
	private void renderQCtoPanel(JPanel panel)
	{
		panel.removeAll(); //clear the panel;
		panel.setLayout(new GridLayout(QC.getLayerSize(),QC.getLayerCount())); //set up the panel 
		
		//for each row of the quantum circuit, draw the elements
		for(int i=0; i<QC.getLayerSize(); i++)
		{
			//for each column in this row, draw the element;
			for(int j=0; j<QC.getLayerCount(); j++)
			{
				JLabel compLabel = new JLabel();
				if (j == cursorxpos && i == cursorypos)
					compLabel.setForeground(Color.red);
				
				if(QC.getGateAt(i, j) != null)
					compLabel.setText(QC.getGateAt(i, j).toString());
				else
					compLabel.setText("NULL");
				panel.add(compLabel);
			}
		}
		this.pack();
	}
	
	
	public static void main(String[] args) 
	{
		JFrame window = new QLSim();
	}

}
