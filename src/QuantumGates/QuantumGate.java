package QuantumGates;

import MathHelpers.Matrix;

public abstract class QuantumGate {
	
	public abstract Matrix getMatrix();
	
}
