package QuantumGates;

import MathHelpers.ComplexNumber;
import MathHelpers.Matrix;

public class HadamardGate extends QuantumGate{
	private Matrix gateMatrix;
	
	public HadamardGate()
	{
		this.gateMatrix = generateGateMatrix(1);
	}
	
	public HadamardGate(int m)
	{
		this.gateMatrix = generateGateMatrix(m);
	}
	
	private Matrix generateGateMatrix(int m)
	{
		if(m < 1) throw new IllegalArgumentException();
		Matrix result = null;
		if(m == 1)
		{
			result = new Matrix(2,2);
			result.setComponent(0, 0, new ComplexNumber(1/Math.sqrt(2),0));
			result.setComponent(0, 1, new ComplexNumber(1/Math.sqrt(2),0));
			result.setComponent(1, 0, new ComplexNumber(1/Math.sqrt(2),0));
			result.setComponent(1, 1, new ComplexNumber(-1/Math.sqrt(2),0));
		}
		else
		{
			result = new Matrix(2,2);
			result.setComponent(0, 0, new ComplexNumber(1/Math.sqrt(2),0));
			result.setComponent(0, 1, new ComplexNumber(1/Math.sqrt(2),0));
			result.setComponent(1, 0, new ComplexNumber(1/Math.sqrt(2),0));
			result.setComponent(1, 1, new ComplexNumber(-1/Math.sqrt(2),0));
			
			//Hm = 1/root(2)[[Hm-1, Hm-1],[Hm-1, -Hm-1]]
			result = result.tensor(generateGateMatrix(m-1));
		}
		return result;
	}


	@Override
	public Matrix getMatrix(){
		return this.gateMatrix;
	}
	
	public String toString()
	{
		return "H";
	}
}
