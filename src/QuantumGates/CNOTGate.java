package QuantumGates;

import MathHelpers.ComplexNumber;
import MathHelpers.Matrix;

public class CNOTGate extends QuantumGate{
	private Matrix gateMatrix;
	
	public CNOTGate()
	{
		this.gateMatrix = new Matrix(4,4);
		this.gateMatrix.setComponent(0, 0, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(0, 1, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(0, 2, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(0, 3, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 0, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 1, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(1, 2, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 3, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(2, 0, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(2, 1, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(2, 2, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(2, 3, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(3, 0, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(3, 1, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(3, 2, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(3, 3, new ComplexNumber(0,0));
	}
	
	
	@Override
	public Matrix getMatrix() {
		return this.gateMatrix;
	}
	
	public String toString()
	{
		return "CNOT";
	}
}
