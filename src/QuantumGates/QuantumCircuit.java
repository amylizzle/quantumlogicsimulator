package QuantumGates;
import java.util.ArrayList;

import MathHelpers.*;
import MathHelpers.Matrix.NonCommutativeException;


public class QuantumCircuit {
	//this should basically be a 2D array of quantum gates - there should be no null entries
	//to allow for dynamic resizing, implement this as arraylist of arraylist of q gate
	
	private ArrayList<ArrayList<QuantumGate>> circuit;
	
	public QuantumCircuit()
	{
		circuit = new ArrayList<ArrayList<QuantumGate>>();
		circuit.add(new ArrayList<QuantumGate>());
		circuit.get(0).add(null);
	}
	
	public QuantumGate getGateAt(int row, int col)
	{
		return this.circuit.get(col).get(row);
	}
	
	public void setGateAt(int row, int col, QuantumGate q)
	{
		int height = q.getMatrix().getRows()/2;
		if(height+row > this.getLayerSize())
			throw new IllegalArgumentException();
		//make sure that if this gate is overriding a CNOT or something else with more than one input, that all instances of the overridden
		//gate are taken out of the circuit. 
		for(int j = row; j < row+height; j++)
		{
			if(this.circuit.get(col).get(j) != null)
			{
				//there's something here - check its height
				QuantumGate tmp = this.circuit.get(col).get(j);
				if(tmp.getMatrix().getRows()/2 > 1)
				{
					//it's taller than 1 - just go through the whole layer and null any refs to it - not fast, but simple
					for(int i = 0; i<this.getLayerSize(); i++)
					{
						if(this.circuit.get(col).get(i) == tmp)
						{
							this.circuit.get(col).set(i, null);
						}
					}
				}
			}
		}
		for(int i = row; i < row+height; i++)
		{
			this.circuit.get(col).set(i, q);
		}
	}
	
	public int getLayerSize()
	{
		return this.circuit.get(0).size();
	}
	
	public int getLayerCount()
	{
		return this.circuit.size();
	}
	
	public void addRow()
	{
		for(int i=0; i<this.circuit.size(); i++)
			this.circuit.get(i).add(null);
	}
	
	public void addLayer()
	{
		ArrayList<QuantumGate> newLayer = new ArrayList<QuantumGate>();
		for(int i=0; i<getLayerSize(); i++)
			newLayer.add(null);
		circuit.add(newLayer);
	}
	
	public void setInputs(Qubit[] inputs) //special case of addLayer
	{
		ArrayList<QuantumGate> newLayer = new ArrayList<QuantumGate>();
		for(int i=0; i<inputs.length; i++)
			newLayer.add(inputs[i]);		
		circuit.set(0,newLayer);
	}
	
	public void addLayer(QuantumGate[] layer)
	{
		if(layer.length != circuit.get(circuit.size()-1).size())
			throw new IllegalArgumentException();
		else
		{
			ArrayList<QuantumGate> newLayer = new ArrayList<QuantumGate>();
			for(int i=0; i<layer.length; i++)
				newLayer.add(layer[i]);
			circuit.add(newLayer);
		}	
	}
	
	public Matrix getOutputState() throws NonCommutativeException
	{
		ArrayList<Matrix> circuitTensorProducts = new ArrayList<Matrix>();
		
		for(int i=0; i<circuit.size(); i++) //iterate through the layers of the circuit, starting at T1
		{
			Matrix layerTensorProduct = new Matrix(1,1); //[1] - because tensor product of [1](x)A = A
			layerTensorProduct.setComponent(0, 0, new ComplexNumber(1,0));
			for(int j=0; j<circuit.get(i).size(); j+=circuit.get(i).get(j).getMatrix().getRows()/2)
			{ 
				layerTensorProduct = layerTensorProduct.tensor(circuit.get(i).get(j).getMatrix());
			}
			circuitTensorProducts.add(0, layerTensorProduct);
		}
		
		Matrix result = circuitTensorProducts.get(0);
		for(int i=1; i<circuitTensorProducts.size(); i++)
		{
			result = result.multiply(circuitTensorProducts.get(i));
		}
		return result;
	}
	
	public String getFriendlyOutputState() throws NonCommutativeException
	{
		Matrix circuitResult = this.getOutputState();
		//matrix will be 2^n where n is number of input qubits
		int qubitcount = (int) (Math.log(circuitResult.getRows())/Math.log(2)); //log(x)/log(2) = log base 2 of x
		String result = "";
		String[] baseStates = new String[circuitResult.getRows()];
		for(int i=0; i<circuitResult.getRows(); i++)
		{
			if(circuitResult.getComponent(i, 0).getRealComponent() != 0 || circuitResult.getComponent(i,0).getImaginaryComponent() != 0)
			{
				String unpadded = Integer.toBinaryString(i); 
				String padding = "";
				for(int x=0; x<qubitcount; x++) { padding = padding + "0"; }
				String padded = padding.substring(unpadded.length()) + unpadded;
				baseStates[i] = "|"+padded+">";
				if(result.length() > 0) { result += " + "; }
				result += "("+circuitResult.getComponent(i, 0)+")"+baseStates[i];
			}
		}
		
		
		return result;
	}
}
