package QuantumGates;

import java.util.Random;

import MathHelpers.ComplexNumber;
import MathHelpers.Matrix;

public class Qubit extends QuantumGate{

	private Matrix value;
	
	public Qubit()
	{
		this.value = new Matrix(2,1);
	}
	
	public Qubit(ComplexNumber zero, ComplexNumber one)
	{
		this.value = new Matrix(2,1);
		this.value.setComponent(0,0, zero);
		this.value.setComponent(1,0, one);
	}
		
	public Qubit(Matrix m)
	{
		if(m.getColumns() != 1 && m.getRows() != 2)
			throw new IllegalArgumentException();
		this.value = m;
	}
	

	
	public void setZeroComponent(ComplexNumber c)
	{
		this.value.setComponent(0,0, c);
	}
	
	public void setOneComponent(ComplexNumber c)
	{
		this.value.setComponent(1,0, c);
	}
	
	public ComplexNumber getZeroComponent()
	{
		return this.value.getComponent(0,0);
	}
	
	public ComplexNumber getOneComponent()
	{
		return this.value.getComponent(1,0);
	}
	
	public Qubit Measure()
	{
		//collapse the wave function of the Qubit into a base state
		Random r = new Random();
		double p = r.nextDouble();
		if(p < (this.getZeroComponent().multiply(this.getZeroComponent().conjugate())).getRealComponent()) //anything * its conjugate is real
		{
			return new Qubit(new ComplexNumber(1,0), new ComplexNumber(0,0));
		}
		else
		{
			return new Qubit(new ComplexNumber(0,0), new ComplexNumber(1,0));
		}
	}
	
	public String toString()
	{
		if(this.getZeroComponent().getRealComponent() == 1) { return "|0>"; }

		if(this.getOneComponent().getRealComponent() == 1) { return "|1>"; }
		
		return "("+this.getZeroComponent()+")|0> + ("+this.getOneComponent()+")|1>";
	}


	@Override
	public Matrix getMatrix() {
		return this.value;
	}
}
