package QuantumGates;

import MathHelpers.ComplexNumber;
import MathHelpers.Matrix;

public class Wire extends QuantumGate{
	private Matrix gateMatrix;
	
	public Wire() //it's just the identity matrix
	{
		this.gateMatrix = new Matrix(2,2);
		this.gateMatrix.setComponent(0, 0, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(0, 1, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 0, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 1, new ComplexNumber(1,0));
	}

	@Override
	public Matrix getMatrix() {
		return this.gateMatrix;
	}
	
	public String toString()
	{
		return "---";
	}
}
