package QuantumGates;

import MathHelpers.ComplexNumber;
import MathHelpers.Matrix;

public class TGate extends QuantumGate{
	private Matrix gateMatrix;
	
	public TGate()
	{
		this.gateMatrix = new Matrix(2,2);
		this.gateMatrix.setComponent(0, 0, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(0, 1, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 0, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 1, new ComplexNumber(1/Math.sqrt(2),-1/Math.sqrt(2))); //e^ipi/4 = 1/sqrt(2) - i/sqrt(2)
	}
	
	@Override
	public Matrix getMatrix() {
		return this.gateMatrix;
	}
	
	public String toString()
	{
		return "T";
	}
}
