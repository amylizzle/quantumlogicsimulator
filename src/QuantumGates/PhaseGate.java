package QuantumGates;

import MathHelpers.ComplexNumber;
import MathHelpers.Matrix;

public class PhaseGate extends QuantumGate{
	private Matrix gateMatrix;
	
	public PhaseGate()
	{
		this.gateMatrix = new Matrix(2,2);
		this.gateMatrix.setComponent(0, 0, new ComplexNumber(1,0));
		this.gateMatrix.setComponent(0, 1, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 0, new ComplexNumber(0,0));
		this.gateMatrix.setComponent(1, 1, new ComplexNumber(0,1));
	}

	@Override
	public Matrix getMatrix() {
		return this.gateMatrix;
	}
	
	public String toString()
	{
		return "Phase";
	}
}
