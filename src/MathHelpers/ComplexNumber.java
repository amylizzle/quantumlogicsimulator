package MathHelpers; 

public class ComplexNumber  
{  
   private double real;  
   private double imaginary;  
  
   public ComplexNumber(double real, double imaginary)  
   {  
      this.real = real;  
      this.imaginary = imaginary;  
   }  
  
   public double getRealComponent()
   {
	   return this.real;
   }
   
   public double getImaginaryComponent()
   {
	   return this.imaginary;
   }
   
   
   public ComplexNumber add(ComplexNumber c)  
   {  
       return new ComplexNumber(this.real + c.real, this.imaginary + c.imaginary);  
   }  

   public ComplexNumber multiply(ComplexNumber c)
   {
	   return new ComplexNumber(this.real*c.real - this.imaginary*c.imaginary, this.real*c.imaginary + this.imaginary*c.real);
   }
   
   public ComplexNumber conjugate()
   {
	   return new ComplexNumber(this.real, -this.imaginary);
   }
   
   public String toString()
   {
	   String result = ""+this.real;
	   if(this.imaginary < 0)
	   {
		   result = result + this.imaginary+"i";
	   }
	   else
	   {
		   result = result +"+"+this.imaginary+"i";
	   }
	   return result;
   }
   
} 
