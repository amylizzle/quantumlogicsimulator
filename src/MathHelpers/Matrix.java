package MathHelpers; 

public class Matrix {

	private int rows,columns;
	private ComplexNumber[][] components;
	
	/////////////EXCEPTIONS////////////////////////
	
	public class NonCommutativeException extends Exception
	{
		private static final long serialVersionUID = -3821087341815353243L;
		
		public NonCommutativeException()
		{
			super();
		}
	}
	
	
	
	/////////////FUNCTION DEFINITIONS//////////////
	
	public Matrix(int r, int c)
	{
		this.rows = r;
		this.columns =c;
		this.components = new ComplexNumber[this.rows][this.columns];
		for(int y=0; y<this.getRows(); y++)
		{
			for(int x=0; x<this.getColumns(); x++)
			{
				this.setComponent(y, x, new ComplexNumber(0,0));
			}
		}
	}
	
	public int getRows()
	{
		return this.rows;
	}
	
	public int getColumns()
	{
		return this.columns;
	}
	
	public ComplexNumber getComponent(int c)
	{
		return this.getComponent(0,c);
	}
	
	public ComplexNumber getComponent(int r, int c)
	{
		return this.components[r][c];
	}
	
	public void setComponent(int r, int c, ComplexNumber value)
	{
		this.components[r][c]=value;
	}
	
	public Matrix add(Matrix m) throws NonCommutativeException
	{
		if(this.getColumns() != m.getColumns() || this.getRows() != m.getRows())
			throw new NonCommutativeException();
		
		Matrix result = new Matrix(this.getColumns(), this.getRows());
		for(int r=0; r<this.getRows(); r++)
		{
			for(int c=0; c<this.getColumns(); c++)
			{
				result.setComponent(r, c, this.getComponent(r,c).add(m.getComponent(r,c)));
			}
		}
		return result;
	}
	
	public Matrix multiply(Matrix m) throws NonCommutativeException
	{
		if(this.getColumns() != m.getRows())
			throw new NonCommutativeException();
		
		Matrix result = new Matrix(this.getRows(),m.getColumns());
		for(int y=0; y<this.getRows(); y++) //for each row of A
		{
			for(int x=0; x<m.getColumns(); x++) //down each column of B
			{
				ComplexNumber value = new ComplexNumber(0,0);
				
				for(int k=0; k<this.getColumns(); k++)
				{
					value = value.add(this.getComponent(y, k).multiply(m.getComponent(k,x)));
				}
				result.setComponent(y, x, value);
			}
		}
		return result;
	}
	
	public Matrix tensor(Matrix m)
	{
		Matrix result = new Matrix(this.getRows()*m.getRows(),this.getColumns()*m.getColumns());
		for(int r=0; r<this.getRows(); r++)
		{
			for(int c=0; c<this.getColumns(); c++)
			{
				for(int r2=0; r2<m.getRows(); r2++)
				{
					for(int c2=0; c2<m.getColumns(); c2++)
					{
						result.setComponent((r*m.getRows())+r2, (c*m.getColumns())+c2, this.getComponent(r,c).multiply(m.getComponent(r2,c2)));
					}
				}
			}
		}
		return result;
	}
	
	public String toString()
	{
		String result = "";
		for(int r=0; r<this.getRows(); r++)
		{
			result = result+"|[";
			for(int c=0; c<this.getColumns(); c++)
			{
				result = result + this.getComponent(r, c);
				if(c != this.getColumns()-1)
				{
					result = result +", ";
				}
			}
			result = result+"]";
			if(r != this.getRows()-1)
			{
				result = result +"|\n";
			}
			else
			{
				result = result+"|";
			}
		}
		return result;
	}
}
